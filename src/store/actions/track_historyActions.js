import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const SEND_TRACK_HISTORY_SUCCESS = 'SEND_TRACK_HISTORY_SUCCESS';

export const fetchTrackHistorySuccess = tracks => ({type: FETCH_TRACK_HISTORY_SUCCESS, tracks});
export const sendTrackHistorySuccess = () => ({type: SEND_TRACK_HISTORY_SUCCESS});

export const fetchTrackHistory = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.get('/track_history', {headers: {'Token': user.token}}).then(
                response =>{
                    console.log(response.data);
                    dispatch(fetchTrackHistorySuccess(response.data))
                }

            );
        }


    };
};

export const sendTrackHistory = track => {
    return (dispatch, getState) => {
        const user = getState().users.user;
            if(user === null){
                dispatch(push('/login'));
            }else{
                return axios.post('/track_history', {trackId: track}, {headers: {'Token': user.token}}).then(
                    () => {
                        dispatch(sendTrackHistorySuccess());
                        dispatch(fetchTrackHistory());
                    }
                );
            }




    };
};
