import axios from '../../axios-api';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';

export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});

export const fetchAlbums = (id) => {
    return dispatch => {
        return axios.get(`/albums/?artist=${id}`).then(
            response => dispatch(fetchAlbumsSuccess(response.data))
        );
    };
};

export const createAlbum = albumData => {
    return dispatch => {
        return axios.post('/albums', albumData).then(
            () => dispatch(createAlbumSuccess())
        );
    };
};
