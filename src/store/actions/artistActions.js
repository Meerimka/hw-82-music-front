import axios from '../../axios-api';
import {push} from "connected-react-router";


export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';

export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});

export const fetchArtists = () => {
    return dispatch => {
        return axios.get('/artists').then(
            response => dispatch(fetchArtistsSuccess(response.data))
        );
    };
};

export const createArtist = artistData => {
    return (dispatch, getState) => {

        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.post('/artists', artistData, {headers: {'Token': user.token}}).then(
                () => {
                    dispatch(createArtistSuccess());
                    dispatch(fetchArtists());
                }
            );
        }

    };
};
