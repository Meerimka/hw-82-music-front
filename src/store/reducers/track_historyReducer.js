import {FETCH_TRACK_HISTORY_SUCCESS} from "../actions/track_historyActions";


const initialState = {
    tracks: null,
};

const track_historyReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, tracks: action.tracks};
        default:
            return state;
    }

};

export default track_historyReducer;