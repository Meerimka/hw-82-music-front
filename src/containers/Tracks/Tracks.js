import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import TrackListItem from "../../components/ListItems/TrackListItem";
import {fetchTracks} from "../../store/actions/TrackActions";
import {sendTrackHistory} from "../../store/actions/track_historyActions";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";



class Tracks extends Component {

    state={
        selectedItem:null
    };

    componentDidMount() {
        this.props.onFetchTracks(this.props.match.params.id);
    }



    showModal = artist =>{
        this.setState({selectedItem: artist})
    };

    hideModal = () =>{
        this.setState({selectedItem: null})
    };



    render() {
        return (
            <Fragment>
                <h2>
                    Tracks
                </h2>
                {this.props.tracks.map(track => (
                    <TrackListItem
                        key={track._id}
                        showModal ={()=>this.showModal(track)}
                        _id={track._id}
                        title={track.title}
                        album={track.album.title}
                        duration={track.duration}
                        number={track.number}
                        youtubeSrc ={track.youtubeSrc}
                        sendTrack={() => this.props.sendTrackHistory(track._id)}
                    />
                ))}

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                            <ModalHeader toggle={this.hideModal}>{this.state.selectedItem.title} </ModalHeader>
                            <ModalBody>

                                <div>
                                    <iframe width="450" height="315" src={this.state.selectedItem.youtubeSrc}
                                            frameBorder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowFullScreen>
                                        {this.state.selectedItem.title}

                                    </iframe>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                            </ModalFooter>
                        </Fragment>
                    )}

                </Modal>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.tracks.tracks,
});

const mapDispatchToProps = dispatch => ({
    onFetchTracks: (id) => dispatch(fetchTracks(id)),
    sendTrackHistory: track => dispatch(sendTrackHistory(track))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);


