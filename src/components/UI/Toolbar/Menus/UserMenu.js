import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {Link} from "react-router-dom";
import {apiURL} from "../../../../constants";


const style = {
    width: '50px',
    height: '50px',
    marginRight: '10px',
    borderRadius:' 50%'
};


const UserMenu = ({user,logout}) => {
    let BASE_URL = apiURL + '/uploads/';
    let src = BASE_URL + user.avatar;
    if (user.facebookId) src = user.avatar;
    return ( <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                <img src={src} style={style} className="img-thumbnail" alt="Something here" />
                Hello, {user.displayName}!
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    <Link to="/track-history">
                        Track history
                    </Link>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
    );
};




export default UserMenu;