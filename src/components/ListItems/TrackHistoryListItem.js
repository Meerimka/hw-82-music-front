import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";


const TrackHistoryListItem = props => {
    return (
        <Card style={{'marginTop': '10px', 'width':"33,3333%"}} >
            <CardBody>
                <h2><i className ="text-muted">Title: &nbsp;</i>
                    <span>
                   {props.title}
                </span>
                </h2>
                <p>
                    <span className="text-muted" >Artist : &nbsp;</span> {props.artist}
                </p>
                <span className="text-muted">listened at : &nbsp;</span> {props.dateTime}
            </CardBody>
        </Card>
    );
};

TrackHistoryListItem.propTypes ={
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.string.isRequired,
    dateTime: PropTypes.string,
};
export default TrackHistoryListItem;