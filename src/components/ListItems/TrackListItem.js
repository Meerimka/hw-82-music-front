import React from 'react';
import PropTypes from 'prop-types';
import {Button, Card, CardBody} from "reactstrap";


const TrackListItem = props => {
    return (
        <Card style={{'marginTop': '10px', 'width':"33,3333%"}}  >
            <CardBody>
                <h2><i className ="text-muted">Title: &nbsp;</i>
                <span>
                   {props.title}
                </span>
                </h2>
                <p>
                   <span className="text-muted" >Album : &nbsp;</span> {props.album}
                </p>
                <span className="text-muted">Duration : &nbsp;</span> {props.duration}
                <p>
                <span className="text-muted">Track-Number : &nbsp;</span> {props.number}
            </p>
                {props.youtubeSrc ? <Button color="danger" size="sm" onClick={props.showModal}>
                    View the video
                </Button> : null}

                <Button size="sm" onClick={props.sendTrack}>
                    Send to history
                </Button>
            </CardBody>
        </Card>
    );
};

TrackListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    album: PropTypes.string.isRequired,
    duration: PropTypes.string,
    number: PropTypes.number,
    sendTrack: PropTypes.func,
    showModal: PropTypes.func.isRequired,
    youtubeSrc: PropTypes.string
};
export default TrackListItem;